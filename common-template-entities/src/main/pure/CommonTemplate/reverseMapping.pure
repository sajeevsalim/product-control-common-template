###Mapping
Mapping CommonTemplate::reverseMapping
(
  *CommonTemplate::EqOptionTemplate: Pure
  {
    ~src cdm::model::TradeState
    optionStrike: $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.strike.strikePrice.amount->toOne(),
    currency: $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.strike.strikePrice.unitOfAmount.currency->toOne(),
    optionExpiryDate: if(
  $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.optionStyle.europeanExercise->isNotEmpty(),
  |$src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.optionStyle.europeanExercise.expirationDate.adjustableDate.unadjustedDate->toOne(),
  |if(
    $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.optionStyle.americanExercise->isNotEmpty(),
    |$src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.optionStyle.americanExercise.expirationDate.adjustableDate.unadjustedDate->toOne(),
    |[]->toOne()
  )
),
    assetType: EnumerationMapping CommonTemplate_AssetTypeEnum: if(
  $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.index->isNotEmpty()->toOne(),
  |'Index',
  |if(
    $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.security->isNotEmpty()->toOne(),
    |'Stock',
    |[]->toOne()
  )
),
    submitterData: $src.trade.tradableProduct.priceQuantity.price.amount->toOne(),
    optionStyle: EnumerationMapping CommonTemplate_OptionStyleEnum: if(
  $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.optionStyle.europeanExercise->isNotEmpty(),
  |'European',
  |if(
    $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.optionStyle.americanExercise->isNotEmpty(),
    |'American',
    |'Bermudan'
  )
),
    underlyingIdentifier: if(
  $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.index->isNotEmpty(),
  |$src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.index.productIdentifier.identifier->toOne(),
  |if(
    $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.security->isNotEmpty(),
    |$src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.security.productIdentifier.identifier->toOne(),
    |[]->toOne()
  )
),
    optionType: EnumerationMapping CommonTemplate_VendorOptionTypeEnum: $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.optionType->toOne(),
    pricingDate: $src.trade.tradeDate,
    instrumentDerivative: EnumerationMapping CommonTemplate_DerivativeEnum: if(
  $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout->isNotEmpty()->toOne(),
  |'Option',
  |if(
    $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.forwardPayout->isNotEmpty()->toOne(),
    |'Forward',
    |'VarSwap'
  )
),
    submitterId: $src.trade.tradeIdentifier.assignedIdentifier.identifier->toOne(),
    quoteType: EnumerationMapping CommonTemplate_QuoteTypeEnum: $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.exerciseTerms.settlement.cashSettlementTerms.cashPriceMethod.quotationRateType->toOne(),
    identifierSourceType: EnumerationMapping CommonTemplate_identifierSourceTypeEnum: if(
  $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.index->isNotEmpty(),
  |$src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.index.productIdentifier.source->toOne(),
  |if(
    $src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.security->isNotEmpty(),
    |$src.trade.tradableProduct.product.contractualProduct.economicTerms.payout.optionPayout.underlier.underlyingProduct.security.productIdentifier.source->toOne(),
    |[]->toOne()
  )
),
    dataType: 'Price'
  }

  CommonTemplate::AssetTypeEnum: EnumerationMapping
  {
    EqIndex: ['Index'],
    EqStock: ['Stock']
  }
  CommonTemplate::OptionStyleEnum: EnumerationMapping
  {
    American: ['American'],
    European: ['European'],
    Bermuda: ['Bermuda']
  }
  CommonTemplate::VendorOptionTypeEnum: EnumerationMapping
  {
    Call: [cdm::model::OptionTypeEnum.Call],
    Put: [cdm::model::OptionTypeEnum.Put]
  }
  CommonTemplate::DerivativeEnum: EnumerationMapping
  {
    Option: ['Option'],
    VarSwap: ['VarSwap'],
    Forward: ['Forward'],
    VolSwap: ['VolSwap'],
    DivSwap: ['DivSwap']
  }
  CommonTemplate::QuoteTypeEnum: EnumerationMapping
  {
    Bid: [cdm::model::QuotationRateTypeEnum.Bid],
    Ask: [cdm::model::QuotationRateTypeEnum.Ask],
    Mid: [cdm::model::QuotationRateTypeEnum.Mid]
  }
  CommonTemplate::identifierSourceTypeEnum: EnumerationMapping
  {
    BBGID: [cdm::model::ProductIdTypeEnum.BBGID],
    BBGTICKER: [cdm::model::ProductIdTypeEnum.BBGTICKER],
    CUSIP: [cdm::model::ProductIdTypeEnum.CUSIP],
    FIGI: [cdm::model::ProductIdTypeEnum.FIGI],
    ISIN: [cdm::model::ProductIdTypeEnum.ISIN],
    Other: [cdm::model::ProductIdTypeEnum.Other],
    SEDOL: [cdm::model::ProductIdTypeEnum.SEDOL]
  }

  MappingTests
  [
    test_2
    (
      query: |CommonTemplate::EqOptionTemplate.all()->graphFetchChecked(
  #{
    CommonTemplate::EqOptionTemplate{
      instrumentDerivative,
      submitterId,
      pricingDate,
      currency,
      submitterData,
      optionType,
      optionStyle,
      optionExpiryDate,
      assetType,
      underlyingIdentifier,
      optionStrike,
      dataType,
      identifierSourceType
    }
  }#
)->serialize(
  #{
    CommonTemplate::EqOptionTemplate{
      instrumentDerivative,
      submitterId,
      pricingDate,
      currency,
      submitterData,
      optionType,
      optionStyle,
      optionExpiryDate,
      assetType,
      underlyingIdentifier,
      optionStrike,
      dataType,
      identifierSourceType
    }
  }#
);
      data:
      [
        <Object, JSON, cdm::model::TradeState, '{"trade":{"tradeIdentifier":[{"issuer":"vendor 1","assignedIdentifier":[{"identifier":"client 123","version":55}]}],"tradeDate":"2021-04-09","tradableProduct":{"product":{"contractualProduct":{"productIdentification":[],"productTaxonomy":[],"economicTerms":[{"effectiveDate":[],"terminationDate":[],"dateAdjustments":[],"payout":{"interestRatePayout":[],"creditDefaultPayout":[],"equityPayout":[],"optionPayout":[{"payoutQuantity":{},"buyerSeller":{"buyer":"Party2","seller":"Party1"},"optionType":"Call","feature":{},"denomination":{"optionEntitlement":0.14115987229160964,"entitlementCurrency":"entitlementCurrency 10","numberOfOptions":0.7307423329912126},"exerciseTerms":{"optionStyle":{"europeanExercise":{"expirationDate":{"adjustableDate":{"unadjustedDate":"2021-06-08"}}}},"strike":{"strikePrice":{"amount":90,"priceType":"NetPrice","unitOfAmount":{"currency":"GBP"}}}},"settlement":{"cashSettlementTerms":{"cashPriceMethod":{"quotationRateType":"Ask","cashSettlementCurrency":"GBP"}}},"underlier":{"underlyingProduct":{"index":{"productIdentifier":{"identifier":"ABC123","source":"FIGI"},"indexType":"Equity"}}}}],"forwardPayout":[],"securityPayout":[],"cashflow":[]},"earlyTerminationProvision":[],"optionProvision":[],"extraordinaryEvents":[],"calculationAgent":[]}]},"index":[],"loan":[],"foreignExchange":[],"commodity":[],"security":[]},"priceQuantity":[{"price":[{"amount":0.9933924677316099,"unitOfAmount":{"capacityUnit":[],"weatherUnit":[],"financialUnit":[],"currency":"GBP"},"priceType":"ReferencePrice","perUnitOfAmount":{"capacityUnit":"INGOT","weatherUnit":"HDD","financialUnit":"IndexUnit","currency":"currency 61"}}],"quantity":[{"amount":0.34571974421851337,"unitOfAmount":{"capacityUnit":[],"weatherUnit":[],"financialUnit":"IndexUnit","currency":[]},"multiplier":0.31308787502348423,"multiplierUnit":{"capacityUnit":"TEU","weatherUnit":"CDD","financialUnit":"Shares","currency":"currency 15"}}],"observable":{"rateOption":[],"commodity":[],"productIdentifier":[{"identifier":"identifier 63","source":"SEDOL"}],"currencyPair":[]}}],"counterparty":[{"role":"Party2","partyReference":{"partyId":["partyId 59"],"name":"name 62","person":[],"account":[]}}],"ancillaryParty":[],"settlementTerms":[],"adjustment":[]},"party":[],"partyRole":[],"settlementTerms":[],"executionDetails":[],"contractDetails":[],"clearedDate":[],"collateral":[],"account":[]},"state":[],"resetHistory":[],"transferHistory":[]}'>
      ];
      assert: '{"defects":[],"source":{"defects":[],"source":{"number":1,"record":"{\"trade\":{\"tradeIdentifier\":[{\"issuer\":\"vendor 1\",\"assignedIdentifier\":[{\"identifier\":\"client 123\",\"version\":55}]}],\"tradeDate\":\"2021-04-09\",\"tradableProduct\":{\"product\":{\"contractualProduct\":{\"productIdentification\":[],\"productTaxonomy\":[],\"economicTerms\":[{\"effectiveDate\":[],\"terminationDate\":[],\"dateAdjustments\":[],\"payout\":{\"interestRatePayout\":[],\"creditDefaultPayout\":[],\"equityPayout\":[],\"optionPayout\":[{\"payoutQuantity\":{},\"buyerSeller\":{\"buyer\":\"Party2\",\"seller\":\"Party1\"},\"optionType\":\"Call\",\"feature\":{},\"denomination\":{\"optionEntitlement\":0.14115987229160964,\"entitlementCurrency\":\"entitlementCurrency 10\",\"numberOfOptions\":0.7307423329912126},\"exerciseTerms\":{\"optionStyle\":{\"europeanExercise\":{\"expirationDate\":{\"adjustableDate\":{\"unadjustedDate\":\"2021-06-08\"}}}},\"strike\":{\"strikePrice\":{\"amount\":90,\"priceType\":\"NetPrice\",\"unitOfAmount\":{\"currency\":\"GBP\"}}}},\"settlement\":{\"cashSettlementTerms\":{\"cashPriceMethod\":{\"quotationRateType\":\"Ask\",\"cashSettlementCurrency\":\"GBP\"}}},\"underlier\":{\"underlyingProduct\":{\"index\":{\"productIdentifier\":{\"identifier\":\"ABC123\",\"source\":\"FIGI\"},\"indexType\":\"Equity\"}}}}],\"forwardPayout\":[],\"securityPayout\":[],\"cashflow\":[]},\"earlyTerminationProvision\":[],\"optionProvision\":[],\"extraordinaryEvents\":[],\"calculationAgent\":[]}]},\"index\":[],\"loan\":[],\"foreignExchange\":[],\"commodity\":[],\"security\":[]},\"priceQuantity\":[{\"price\":[{\"amount\":0.9933924677316099,\"unitOfAmount\":{\"capacityUnit\":[],\"weatherUnit\":[],\"financialUnit\":[],\"currency\":\"GBP\"},\"priceType\":\"ReferencePrice\",\"perUnitOfAmount\":{\"capacityUnit\":\"INGOT\",\"weatherUnit\":\"HDD\",\"financialUnit\":\"IndexUnit\",\"currency\":\"currency 61\"}}],\"quantity\":[{\"amount\":0.34571974421851337,\"unitOfAmount\":{\"capacityUnit\":[],\"weatherUnit\":[],\"financialUnit\":\"IndexUnit\",\"currency\":[]},\"multiplier\":0.31308787502348423,\"multiplierUnit\":{\"capacityUnit\":\"TEU\",\"weatherUnit\":\"CDD\",\"financialUnit\":\"Shares\",\"currency\":\"currency 15\"}}],\"observable\":{\"rateOption\":[],\"commodity\":[],\"productIdentifier\":[{\"identifier\":\"identifier 63\",\"source\":\"SEDOL\"}],\"currencyPair\":[]}}],\"counterparty\":[{\"role\":\"Party2\",\"partyReference\":{\"partyId\":[\"partyId 59\"],\"name\":\"name 62\",\"person\":[],\"account\":[]}}],\"ancillaryParty\":[],\"settlementTerms\":[],\"adjustment\":[]},\"party\":[],\"partyRole\":[],\"settlementTerms\":[],\"executionDetails\":[],\"contractDetails\":[],\"clearedDate\":[],\"collateral\":[],\"account\":[]},\"state\":[],\"resetHistory\":[],\"transferHistory\":[]}"},"value":{"trade":{"tradeIdentifier":[{"assignedIdentifier":[{"identifier":"client 123"}],"issuer":"vendor 1","issuerReference":null}],"clearedDate":null,"partyRole":[],"tradeDate":"2021-04-09","contractDetails":null,"tradableProduct":{"product":{"security":null,"commodity":null,"foreignExchange":null,"contractualProduct":{"economicTerms":{"payout":{"forwardPayout":[],"interestRatePayout":[],"creditDefaultPayout":null,"cashflow":[],"equityPayout":[],"optionPayout":[{"exerciseTerms":{"optionStyle":{"europeanExercise":{"expirationDate":{"adjustableDate":{"unadjustedDate":"2021-06-08","dateAdjustments":null,"dateAdjustmentsReference":null}}},"bermudaExercise":null,"americanExercise":null},"strike":{"referenceSwapCurve":null,"averagingStrikeFeature":null,"strikePrice":{"priceType":"NetPrice","amount":90.0,"unitOfAmount":{"financialUnit":null,"weatherUnit":null,"capacityUnit":null,"frequency":null,"currency":"GBP"}},"strikeReference":null}},"optionType":"Call","underlier":{"underlyingProduct":{"index":{"productIdentifier":[{"identifier":"ABC123","source":"FIGI"}]},"security":null}},"payoutQuantity":{"reset":null,"quantityMultiplier":null,"quantitySchedule":null,"quantityReference":null}}]},"calculationAgent":null,"effectiveDate":null,"terminationDate":null,"extraordinaryEvents":null}},"index":null,"loan":null},"priceQuantity":[{"price":[{"priceType":"ReferencePrice","amount":0.9933924677316099,"unitOfAmount":{"financialUnit":null,"weatherUnit":null,"capacityUnit":null,"frequency":null,"currency":"GBP"}}],"observable":{"productIdentifier":[{}],"rateOption":null,"currencyPair":null,"commodity":null},"quantity":[{"amount":0.34571974421851337,"multiplier":0.31308787502348423}]}]}}}},"value":{"instrumentDerivative":"Option","submitterId":"client 123","pricingDate":"2021-04-09","currency":"GBP","submitterData":0.9933924677316099,"optionType":"Call","optionStyle":"European","optionExpiryDate":"2021-06-08","assetType":"EqIndex","underlyingIdentifier":"ABC123","optionStrike":90.0,"dataType":"Price","identifierSourceType":"FIGI"}}';
    ),
    reverse_mapping
    (
      query: |CommonTemplate::EqOptionTemplate.all()->graphFetchChecked(
  #{
    CommonTemplate::EqOptionTemplate{
      assetType,
      identifierSourceType,
      instrumentDerivative,
      optionStyle,
      optionType,
      currency,
      dataType,
      optionExpiryDate,
      optionStrike,
      pricingDate,
      submitterData,
      submitterId,
      underlyingIdentifier
    }
  }#
)->serialize(
  #{
    CommonTemplate::EqOptionTemplate{
      assetType,
      identifierSourceType,
      instrumentDerivative,
      optionStyle,
      optionType,
      currency,
      dataType,
      optionExpiryDate,
      optionStrike,
      pricingDate,
      submitterData,
      submitterId,
      underlyingIdentifier
    }
  }#
);
      data:
      [
        <Object, JSON, cdm::model::TradeState, '{"trade":{"tradeIdentifier":[{"issuer":"vendor 1","assignedIdentifier":[{"identifier":"client 123","version":55}]}],"tradeDate":"2021-04-09","tradableProduct":{"product":{"contractualProduct":{"economicTerms":[{"payout":{"optionPayout":[{"payoutQuantity":{},"buyerSeller":{"buyer":"Party2","seller":"Party1"},"optionType":"Call","feature":{},"exerciseTerms":{"optionStyle":{"europeanExercise":{"expirationDate":{"adjustableDate":{"unadjustedDate":"2021-06-08"}}}},"strike":{"strikePrice":{"amount":90,"priceType":"NetPrice","unitOfAmount":{"currency":"GBP"}}}},"settlement":{"cashSettlementTerms":{"cashPriceMethod":{"quotationRateType":"Ask","cashSettlementCurrency":"GBP"}}},"underlier":{"underlyingProduct":{"index":{"productIdentifier":{"identifier":"ABC123","source":"FIGI"},"indexType":"Equity"}}}}]}}]}},"priceQuantity":[{"price":[{"amount":0.9933924677316099,"unitOfAmount":{"capacityUnit":[],"weatherUnit":[],"financialUnit":[],"currency":"GBP"},"priceType":"ReferencePrice","perUnitOfAmount":{"capacityUnit":"INGOT","weatherUnit":"HDD","financialUnit":"IndexUnit","currency":"currency 61"}}],"quantity":[{"amount":0.34571974421851337,"unitOfAmount":{"capacityUnit":[],"weatherUnit":[],"financialUnit":"IndexUnit","currency":[]},"multiplier":0.31308787502348423,"multiplierUnit":{"capacityUnit":"TEU","weatherUnit":"CDD","financialUnit":"Shares","currency":"currency 15"}}],"observable":{"rateOption":[],"commodity":[],"productIdentifier":[{"identifier":"identifier 63","source":"SEDOL"}],"currencyPair":[]}}],"counterparty":[{"role":"Party2","partyReference":{"partyId":["partyId 59"],"name":"name 62","person":[],"account":[]}}]}}}'>
      ];
      assert: '{"defects":[],"source":{"defects":[],"source":{"number":1,"record":"{\"trade\":{\"tradeIdentifier\":[{\"issuer\":\"vendor 1\",\"assignedIdentifier\":[{\"identifier\":\"client 123\",\"version\":55}]}],\"tradeDate\":\"2021-04-09\",\"tradableProduct\":{\"product\":{\"contractualProduct\":{\"economicTerms\":[{\"payout\":{\"optionPayout\":[{\"payoutQuantity\":{},\"buyerSeller\":{\"buyer\":\"Party2\",\"seller\":\"Party1\"},\"optionType\":\"Call\",\"feature\":{},\"exerciseTerms\":{\"optionStyle\":{\"europeanExercise\":{\"expirationDate\":{\"adjustableDate\":{\"unadjustedDate\":\"2021-06-08\"}}}},\"strike\":{\"strikePrice\":{\"amount\":90,\"priceType\":\"NetPrice\",\"unitOfAmount\":{\"currency\":\"GBP\"}}}},\"settlement\":{\"cashSettlementTerms\":{\"cashPriceMethod\":{\"quotationRateType\":\"Ask\",\"cashSettlementCurrency\":\"GBP\"}}},\"underlier\":{\"underlyingProduct\":{\"index\":{\"productIdentifier\":{\"identifier\":\"ABC123\",\"source\":\"FIGI\"},\"indexType\":\"Equity\"}}}}]}}]}},\"priceQuantity\":[{\"price\":[{\"amount\":0.9933924677316099,\"unitOfAmount\":{\"capacityUnit\":[],\"weatherUnit\":[],\"financialUnit\":[],\"currency\":\"GBP\"},\"priceType\":\"ReferencePrice\",\"perUnitOfAmount\":{\"capacityUnit\":\"INGOT\",\"weatherUnit\":\"HDD\",\"financialUnit\":\"IndexUnit\",\"currency\":\"currency 61\"}}],\"quantity\":[{\"amount\":0.34571974421851337,\"unitOfAmount\":{\"capacityUnit\":[],\"weatherUnit\":[],\"financialUnit\":\"IndexUnit\",\"currency\":[]},\"multiplier\":0.31308787502348423,\"multiplierUnit\":{\"capacityUnit\":\"TEU\",\"weatherUnit\":\"CDD\",\"financialUnit\":\"Shares\",\"currency\":\"currency 15\"}}],\"observable\":{\"rateOption\":[],\"commodity\":[],\"productIdentifier\":[{\"identifier\":\"identifier 63\",\"source\":\"SEDOL\"}],\"currencyPair\":[]}}],\"counterparty\":[{\"role\":\"Party2\",\"partyReference\":{\"partyId\":[\"partyId 59\"],\"name\":\"name 62\",\"person\":[],\"account\":[]}}]}}}"},"value":{"trade":{"tradeIdentifier":[{"assignedIdentifier":[{"identifier":"client 123"}],"issuer":"vendor 1","issuerReference":null}],"clearedDate":null,"tradableProduct":{"product":{"loan":null,"security":null,"contractualProduct":{"economicTerms":{"terminationDate":null,"extraordinaryEvents":null,"payout":{"forwardPayout":[],"cashflow":[],"equityPayout":[],"interestRatePayout":[],"creditDefaultPayout":null,"optionPayout":[{"exerciseTerms":{"optionStyle":{"europeanExercise":{"expirationDate":{"adjustableDate":{"unadjustedDate":"2021-06-08","dateAdjustments":null,"dateAdjustmentsReference":null}}},"americanExercise":null,"bermudaExercise":null},"strike":{"averagingStrikeFeature":null,"referenceSwapCurve":null,"strikePrice":{"unitOfAmount":{"financialUnit":null,"frequency":null,"currency":"GBP","weatherUnit":null,"capacityUnit":null},"amount":90.0,"priceType":"NetPrice"},"strikeReference":null}},"optionType":"Call","underlier":{"underlyingProduct":{"index":{"productIdentifier":[{"identifier":"ABC123","source":"FIGI"}]},"security":null}},"payoutQuantity":{"reset":null,"quantitySchedule":null,"quantityReference":null,"quantityMultiplier":null}}]},"calculationAgent":null,"effectiveDate":null}},"index":null,"foreignExchange":null,"commodity":null},"priceQuantity":[{"observable":{"commodity":null,"rateOption":null,"productIdentifier":[{}],"currencyPair":null},"price":[{"unitOfAmount":{"financialUnit":null,"frequency":null,"currency":"GBP","weatherUnit":null,"capacityUnit":null},"amount":0.9933924677316099,"priceType":"ReferencePrice"}],"quantity":[{"amount":0.34571974421851337,"multiplier":0.31308787502348423}]}]},"partyRole":[],"tradeDate":"2021-04-09","contractDetails":null}}},"value":{"assetType":"EqIndex","identifierSourceType":"FIGI","instrumentDerivative":"Option","optionStyle":"European","optionType":"Call","currency":"GBP","dataType":"Price","optionExpiryDate":"2021-06-08","optionStrike":90.0,"pricingDate":"2021-04-09","submitterData":0.9933924677316099,"submitterId":"client 123","underlyingIdentifier":"ABC123"}}';
    )
  ]
)
