Class {meta::pure::profiles::doc.doc = 'A class to specify the ISDA SIMM version that applies to the ISDA 2018 CSA for Initial Margin. According to the ISDA 2018 CSA for Initial Margin, Paragraph 13, General Principles (ee) (1) provisions, the SIMM version is either not specified, or references a version used by one of the parties to the agreement.'} cdm::model::SimmVersion
[
  VersionNotSpecified: if(
  !($this.isSpecified->toOne()),
  |$this.partyVersion->isEmpty(),
  |true
),
  VersionSpecified: if(
  $this.isSpecified->toOne(),
  |$this.partyVersion->isNotEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'A boolean attribute to determine whether the SIMM version is specified for the purpose of the legal agreement.'} isSpecified: Boolean[0..1];
  {meta::pure::profiles::doc.doc = 'The party which the specified SIMM version applies to.'} partyVersion: cdm::model::CounterpartyRoleEnum[0..1];
  {meta::pure::profiles::doc.doc = 'The SIMM version exception when specified as a customized approach by the party.'} asSpecified: String[0..1];
}
