Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'Class to specify a cashflow, i.e. the outcome of either of computation (e.g. interest accrual) or an assessment of some sort (e.g. a fee). The cashflow can then be turned into a cash transfer, artefact to be used as the input to a payment system or the outcome of it. The associated globalKey denotes the ability to associate a hash value to the Cashflow instantiations for the purpose of model cross-referencing, in support of functionality such as the event effect and the lineage.'} cdm::model::Cashflow extends cdm::model::PayoutBase
[
  Cashflow_cashflowAmount: if(
  $this.cashflowAmount->isNotEmpty(),
  |$this.cashflowAmount.amount >= 0,
  |true
)
]
{
  payerReceiver: cdm::model::PayerReceiver[1];
  {meta::pure::profiles::doc.doc = 'FpML specifies the FpML PaymentDiscounting.model group for representing the discounting elements that can be associated with a payment.'} paymentDiscounting: cdm::model::PaymentDiscounting[0..1];
  {meta::pure::profiles::doc.doc = 'SCHEDULED FOR DEPRECATION, QUANTITY HANDLED IN PAYOUTBASE. The currency amount of the payment.'} cashflowAmount: cdm::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'FpML specifies the Premium.model group for representing the option premium when expressed in a way other than an amount.'} premiumExpression: cdm::model::PremiumExpression[0..1];
  cashflowDate: cdm::model::AdjustableOrAdjustedOrRelativeDate[0..1];
  {meta::pure::profiles::doc.doc = 'This is a conceptual placeholder for providing the breakdown into the cashflow calculation components, leveraging the fact that the CDM provides calculation components, starting with the FixedAmount and the FloatingAmount. Further evaluation of expected usage needs to take place to confirm and prioritize such implementation.'} cashflowCalculation: Float[0..1];
  {meta::pure::profiles::doc.doc = 'The qualification of the type of cashflow, e.g. brokerage fee, premium, upfront fee etc. Particularly relevant when it cannot be inferred directly through lineage.'} cashflowType: cdm::model::CashflowTypeEnum[0..1];
  {meta::pure::profiles::doc.doc = 'The value representing the discount factor used to calculate the present value of the cashflow.'} discountFactor: Float[0..1];
  {meta::pure::profiles::doc.doc = 'The amount representing the present value of the forecast payment.'} presentValueAmount: cdm::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'Applicable to CDS on MBS to specify whether payment delays are applicable to the fixed Amount. RMBS typically have a payment delay of 5 days between the coupon date of the reference obligation and the payment date of the synthetic swap. CMBS do not, on the other hand, with both payment dates being on the 25th of each month.'} paymentDelay: Boolean[0..1];
}
