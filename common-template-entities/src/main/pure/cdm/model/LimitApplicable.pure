Class cdm::model::LimitApplicable
[
  LimitApplicableChoice: (($this.amountUtilized->isEmpty() &&
  $this.utilization->isEmpty()) ||
  ($this.amountUtilized->isNotEmpty() &&
  $this.utilization->isEmpty())) ||
  ($this.utilization->isNotEmpty() &&
  $this.amountUtilized->isEmpty())
]
{
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'Standard code to indicate which type of credit line is being referred to - i.e. IM, DV01, PV01, CS01, Notional, Clip Size, Notional, maximumOrderQuantity.'} limitType: cdm::model::CreditLimitTypeEnum[0..1];
  {meta::pure::profiles::doc.doc = 'This element is required in FpML, optional in CDM for the purpose of accommodating the CME data representation while making reference to the FpML one.'} clipSize: Integer[0..1];
  {meta::pure::profiles::doc.doc = 'The limit utilised by all the cleared trades for the limit level and limit type. While the attribute is of type integer in FpML and the CME schema, it has been specified to be of type number in the CDM to take into consideration java size limits as well as for consistency purposes with the way most monetary amounts are expressed.'} amountUtilized: Float[0..1];
  utilization: cdm::model::CreditLimitUtilisation[0..1];
  {meta::pure::profiles::doc.doc = 'The limit remaining for the limit level and limit type. This does not take into account any pending trades. While the attribute is of type integer in FpML and the CME schema, it has been specified to be of type number in the CDM to take into consideration java size limits as well as for consistency purposes with the way most monetary amounts are expressed.'} amountRemaining: Float[0..1];
  <<cdm::model::metadata.scheme>> {meta::pure::profiles::doc.doc = 'The currency in which the applicable limit is denominated. The list of valid currencies is not presently positioned as an enumeration as part of the CDM because that scope is limited to the values specified by ISDA and FpML. As a result, implementers have to make reference to the relevant standard, such as the ISO 4217 standard for currency codes.'} currency: String[0..1];
  velocity: cdm::model::Velocity[0..1];
}
