Enum {meta::pure::profiles::doc.doc = 'The enumeration values to specify the type of payment.'} cdm::model::PaymentTypeEnum
{
  {meta::pure::profiles::doc.doc = 'A cash flow associated with an amendment lifecycle event.'} AmendmentFee,
  {meta::pure::profiles::doc.doc = 'A cash flow resulting from the assignment of a contract to a new counterparty.'} AssignmentFee,
  {meta::pure::profiles::doc.doc = 'The brokerage commission.'} BrokerageCommission,
  {meta::pure::profiles::doc.doc = 'A cash flow corresponding to the periodic accrued interests.'} Coupon,
  {meta::pure::profiles::doc.doc = 'A cashflow resulting from a credit event.'} CreditEvent,
  {meta::pure::profiles::doc.doc = 'A cash flow corresponding to the synthetic dividend of an equity underlier asset traded through a derivative instrument.'} DividendReturn,
  {meta::pure::profiles::doc.doc = 'A cash flow associated with an exercise lifecycle event.'} ExerciseFee,
  {meta::pure::profiles::doc.doc = 'A generic term for describing a non-scheduled cashflow that can be associated either with the initial contract, with some later corrections to it (e.g. a correction to the day count fraction that has a cashflow impact) or with some lifecycle events. Fees that are specifically associated with termination and partial termination, increase, amendment, and exercise events are qualified accordingly.'} Fee,
  {meta::pure::profiles::doc.doc = 'A cash flow associated with an increase lifecycle event.'} IncreaseFee,
  {meta::pure::profiles::doc.doc = 'Interest, without qualification as to whether it a gross or net interest relates cashflow.'} Interest,
  {meta::pure::profiles::doc.doc = 'A cash flow corresponding to the return of the interest rate portion of a derivative instrument that has different types of underlying assets, such as a total return swap.'} InterestReturn,
  {meta::pure::profiles::doc.doc = 'Net interest across payout components. Applicable to products such as interest rate swaps.'} NetInterest,
  {meta::pure::profiles::doc.doc = 'The novation fee.'} NovationFee,
  {meta::pure::profiles::doc.doc = 'A cash flow associated with a partial termination lifecycle event.'} PartialTerminationFee,
  {meta::pure::profiles::doc.doc = 'The premium associated with an OTC contract such as an option or a cap/floor.'} Premium,
  {meta::pure::profiles::doc.doc = 'A cash flow corresponding to the return of the price portion of a derivative instrument that has different types of underlying assets, such as a total return swap.'} PriceReturn,
  {meta::pure::profiles::doc.doc = 'A cash flow which amount typically corresponds to the notional of the contract and that is exchanged between the parties on trade inception and reverted back when the contract is terminated.'} PrincipalExchange,
  {meta::pure::profiles::doc.doc = 'A cash flow associated with a termination lifecycle event.'} TerminationFee,
  {meta::pure::profiles::doc.doc = 'An upfront cashflow associated to the swap to adjust for a difference between the swap price and the current market price.'} UpfrontFee,
  NetCashflow
}
