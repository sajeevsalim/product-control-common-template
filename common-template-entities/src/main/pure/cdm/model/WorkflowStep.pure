Class <<cdm::model::metadata.key>> {meta::pure::profiles::doc.doc = 'A workflow step represents the state of a business event. The workflow step contains a reference to a previous WorkflowStep in order to preserve lineage. A workflow step is accepted if it contains a business event, proposed if proposedInstruction is present and is rejected if the rejected flag is set.'} cdm::model::WorkflowStep
[
  WorkflowStepStatus: ((($this.businessEvent->isNotEmpty() &&
  $this.proposedInstruction->isEmpty()) &&
  $this.rejected->isEmpty()) ||
  (($this.businessEvent->isEmpty() &&
  $this.proposedInstruction->isNotEmpty()) &&
  $this.rejected->isEmpty())) ||
  (($this.businessEvent->isEmpty() &&
  $this.proposedInstruction->isEmpty()) &&
  $this.rejected->isNotEmpty()),
  Intent: if(
  (($this.businessEvent.primitives.split->isNotEmpty() ||
    $this.businessEvent.primitives.execution->isNotEmpty()) ||
    $this.businessEvent.primitives.contractFormation->isNotEmpty()) ||
    $this.businessEvent.primitives.reset->isNotEmpty(),
  |$this.businessEvent.intent->isEmpty(),
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'Life cycle event for the step. The businessEvent is optional when a proposedInstruction or rejection are present.'} businessEvent: cdm::model::BusinessEvent[0..1];
  {meta::pure::profiles::doc.doc = 'The proposed instruction for the next workflow step. The proposedInstruction is optional when the businessEvent or rejection are present'} proposedInstruction: cdm::model::Instruction[0..1];
  {meta::pure::profiles::doc.doc = 'Flags this step as rejected.'} rejected: Boolean[0..1];
  <<cdm::model::metadata.reference>> {meta::pure::profiles::doc.doc = 'Optional previous workflow step that provides lineage to workflow steps that precedes it.'} previousWorkflowStep: cdm::model::WorkflowStep[0..1];
  {meta::pure::profiles::doc.doc = 'Contains all information pertaining the FpML messaging header'} messageInformation: cdm::model::MessageInformation[0..1];
  {meta::pure::profiles::doc.doc = 'The set of timestamp(s) associated with the event as a collection of [dateTime, qualifier].'} timestamp: cdm::model::EventTimestamp[1..*];
  {meta::pure::profiles::doc.doc = 'The identifier(s) that uniquely identify a lifecycle event. The unbounded cardinality is meant to provide the ability to associate identifiers that are issued by distinct parties. As an example, each of the parties to the event may choose to associate their own identifiers to the event.'} eventIdentifier: cdm::model::Identifier[1..*];
  {meta::pure::profiles::doc.doc = 'Specifies whether the event is a new, a correction or a cancellation.'} action: cdm::model::ActionEnum[0..1];
  {meta::pure::profiles::doc.doc = 'The specification of the event parties. This attribute is optional, as not applicable to certain events (e.g. most of the observations).'} party: cdm::model::Party[*];
  {meta::pure::profiles::doc.doc = 'Optional account information that could be associated to the event.'} account: cdm::model::Account[*];
  {meta::pure::profiles::doc.doc = 'The lineage attribute provides a linkage among lifecycle events through the globalKey hash value. One example is when a given lifecycle event is being corrected or cancelled. In such case, each subsequent event will have lineage into the prior version of that event. The second broad use case is when an event has a dependency upon either another event (e.g. the regular payment associated with a fix/float swap will have a lineage into the reset event, which will in turn have a lineage into the observation event for the floating rate and the contract) or a contract (e.g. the exercise of an option has a lineage into that option).'} lineage: cdm::model::Lineage[0..1];
  creditLimitInformation: cdm::model::CreditLimitInformation[0..1];
}
