Enum {meta::pure::profiles::doc.doc = 'Specifies the general rule for repayment of principal.'} cdm::model::DebtPrincipalEnum
{
  {meta::pure::profiles::doc.doc = 'Denotes that the principal is paid all at once on maturity of the debt insrument. Bullet debt instruments cannot be redeemed early by an issuer, which means they are non-callable.'} Bullet,
  {meta::pure::profiles::doc.doc = 'Denotes that the principal on the debt can be repaid early, in whole or in part, at the option of the issuer.'} Callable,
  {meta::pure::profiles::doc.doc = 'Denotes that the principal on the debt can be repaid early, in whole or in part, at the option of the holder.'} Puttable,
  {meta::pure::profiles::doc.doc = 'Denotes that the principal on the debt is paid down regularly, along with its interest expense over the life of the debt instrument.  Includes amortising instruments with a bullet balance repayment at maturity.'} Amortising,
  {meta::pure::profiles::doc.doc = 'SDenotes that the principal on the debt is calculated with reference to one or more specified inflation rates.'} InflationLinked,
  {meta::pure::profiles::doc.doc = 'Denotes that the  principal on the debt is calculated with reference to one or more price or other indices (other than inflation rates).'} IndexLinked,
  {meta::pure::profiles::doc.doc = 'Denotes that the  principal on the debt is calculated with reference to other underlyings (not being floating interest rates, inflation rates or indices) or with a non-linear relationship to floating interest rates, inflation rates or indices.'} OtherStructured,
  {meta::pure::profiles::doc.doc = 'Denotes a stripped bond representing only the principal component.'} PrincipalOnly
}
