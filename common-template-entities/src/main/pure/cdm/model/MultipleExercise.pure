Class {meta::pure::profiles::doc.doc = 'A class defining multiple exercises. As defined in the 2000 ISDA Definitions, Section 12.4. Multiple Exercise, the buyer of the option has the right to exercise all or less than all the unexercised notional amount of the underlying swap on one or more days in the exercise period, but on any such day may not exercise less than the minimum notional amount or more than the maximum notional amount, and if an integral multiple amount is specified, the notional exercised must be equal to or, be an integral multiple of, the integral multiple amount. In FpML, MultipleExercise is built upon the PartialExercise.model.'} cdm::model::MultipleExercise extends cdm::model::PartialExercise
[
  one_of_constraint: ($this.maximumNotionalAmount->isNotEmpty() &&
  $this.maximumNumberOfOptions->isEmpty()) ||
  ($this.maximumNumberOfOptions->isNotEmpty() &&
  $this.maximumNotionalAmount->isEmpty()),
  MaximumNumberOfOptions: if(
  $this.maximumNumberOfOptions->isNotEmpty(),
  |$this.maximumNumberOfOptions >= 0.0,
  |true
),
  MinimumNumberOfOptions: if(
  $this.minimumNumberOfOptions->isNotEmpty(),
  |$this.minimumNumberOfOptions >= 0,
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'The maximum notional amount that can be exercised on a given exercise date.'} maximumNotionalAmount: Float[0..1];
  {meta::pure::profiles::doc.doc = 'The maximum number of options that can be exercised on a given exercise date. If the number is not specified, it means that the maximum number of options corresponds to the remaining unexercised options.'} maximumNumberOfOptions: Float[0..1];
}
