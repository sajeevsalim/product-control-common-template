Class {meta::pure::profiles::doc.doc = 'In FpML, PhysicalSettlementTerms and CashSettlementTerms extend SettlementTerms. In the CDM, this extension paradigm has not been used because SettlementTerms class has been used for purposes related to securities transactions, while it is not used as such in the FpML standard (i.e. only as an abstract construct.'} cdm::model::CashSettlementTerms extends cdm::model::SettlementBase
[
  CashSettlementTerms_choice: (($this.cashSettlementAmount->isEmpty() &&
  $this.recoveryFactor->isEmpty()) ||
  ($this.cashSettlementAmount->isNotEmpty() &&
  $this.recoveryFactor->isEmpty())) ||
  ($this.recoveryFactor->isNotEmpty() &&
  $this.cashSettlementAmount->isEmpty()),
  CashSettlementTerms_cashSettlementBusinessDays: if(
  $this.cashSettlementBusinessDays->isNotEmpty(),
  |$this.cashSettlementBusinessDays >= 0,
  |true
),
  CashSettlementTerms_recoveryFactor: if(
  $this.recoveryFactor->isNotEmpty(),
  |($this.recoveryFactor >= 0.0) &&
    ($this.recoveryFactor <= 1.0),
  |true
),
  FpML_cd_37: if(
  ($this.quotationAmount->isNotEmpty() &&
    $this.minimumQuotationAmout->isNotEmpty()) &&
    ($this.quotationAmount.currency ==
    $this.minimumQuotationAmout.currency),
  |$this.quotationAmount.amount >
    $this.minimumQuotationAmout.amount,
  |true
)
]
{
  {meta::pure::profiles::doc.doc = 'The number of business days after conditions to settlement have been satisfied when the calculation agent obtains a price quotation on the Reference Obligation for purposes of cash settlement. There may be one or more valuation dates. This is typically specified if the cash settlement amount is not a fixed amount. ISDA 2003 Term: Valuation Date.'} valuationDate: cdm::model::ValuationDate[0..1];
  {meta::pure::profiles::doc.doc = 'The time of day in the specified business center when the calculation agent seeks quotations for an amount of the reference obligation for purposes of cash settlement. ISDA 2003 Term: Valuation Time.'} valuationTime: cdm::model::BusinessCenterTime[0..1];
  {meta::pure::profiles::doc.doc = 'The type of price quotations to be requested from dealers when determining the market value of the reference obligation for purposes of cash settlement. For example, Bid, Offer or Mid-market. ISDA 2003 Term: Quotation Method.'} quotationMethod: cdm::model::QuotationRateTypeEnum[0..1];
  {meta::pure::profiles::doc.doc = 'In the determination of a cash settlement amount, if weighted average quotations are to be obtained, the quotation amount specifies an upper limit to the outstanding principal balance of the reference obligation for which the quote should be obtained. If not specified, the ISDA definitions provide for a fallback amount equal to the floating rate payer calculation amount. ISDA 2003 Term: Quotation Amount.'} quotationAmount: cdm::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'In the determination of a cash settlement amount, if weighted average quotations are to be obtained, the minimum quotation amount specifies a minimum intended threshold amount of outstanding principal balance of the reference obligation for which the quote should be obtained. If not specified, the ISDA definitions provide for a fallback amount of the lower of either USD 1,000,000 (or its equivalent in the relevant obligation currency) or the quotation amount. ISDA 2003 Term: Minimum Quotation Amount.'} minimumQuotationAmout: cdm::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'A dealer from whom quotations are obtained by the calculation agent on the reference obligation for purposes of cash settlement. ISDA 2003 Term: Dealer.'} dealer: String[0..1];
  {meta::pure::profiles::doc.doc = 'The number of business days used in the determination of the cash settlement payment date. If a cash settlement amount is specified, the cash settlement payment date will be this number of business days following the calculation of the final price. If a cash settlement amount is not specified, the cash settlement payment date will be this number of business days after all conditions to settlement are satisfied. ISDA 2003 Term: Cash Settlement Date.'} cashSettlementBusinessDays: Integer[0..1];
  {meta::pure::profiles::doc.doc = 'The amount paid by the seller to the buyer for cash settlement on the cash settlement date. If not otherwise specified, would typically be calculated as 100 (or the Reference Price) minus the price of the Reference Obligation (all expressed as a percentage) times Floating Rate Payer Calculation Amount. ISDA 2003 Term: Cash Settlement Amount.'} cashSettlementAmount: cdm::model::Money[0..1];
  {meta::pure::profiles::doc.doc = 'Used for fixed recovery, specifies the recovery level, determined at contract formation, to be applied on a default. Used to calculate the amount paid by the seller to the buyer for cash settlement on the cash settlement date. Amount calculation is (1 minus the Recovery Factor) multiplied by the Floating Rate Payer Calculation Amount. The currency will be derived from the Floating Rate Payer Calculation Amount.'} recoveryFactor: Float[0..1];
  {meta::pure::profiles::doc.doc = 'Used for Recovery Lock, to indicate whether fixed Settlement is Applicable or Not Applicable. If Buyer fails to deliver an effective Notice of Physical Settlement on or before the Buyer NOPS Cut-off Date, and if Seller fails to deliver an effective Seller NOPS on or before the Seller NOPS Cut-off Date, then either: (a) if Fixed Settlement is specified in the related Confirmation as not applicable, then the Seller NOPS Cut-off Date shall be the Termination Date; or (b) if Fixed Settlement is specified in the related Confirmation as applicable, then: (i) if the Fixed Settlement Amount is a positive number, Seller shall, subject to Section 3.1 (except for the requirement of satisfaction of the Notice of Physical Settlement Condition to Settlement), pay the Fixed Settlement Amount to Buyer on the Fixed Settlement Payment Date; and (ii) if the Fixed Settlement Amount is a negative number, Buyer shall, subject to Section 3.1 (except for the requirement of satisfaction of the Notice of Physical Settlement Condition to Settlement), pay the absolute value of the Fixed Settlement Amount to Seller on the Fixed Settlement Payment Date.'} fixedSettlement: Boolean[0..1];
  {meta::pure::profiles::doc.doc = 'Indicates whether accrued interest is included (true) or not (false). For cash settlement this specifies whether quotations should be obtained inclusive or not of accrued interest. For physical settlement this specifies whether the buyer should deliver the obligation with an outstanding principal balance that includes or excludes accrued interest. ISDA 2003 Term: Include/Exclude Accrued Interest.'} accruedInterest: Boolean[0..1];
  {meta::pure::profiles::doc.doc = 'The ISDA defined methodology for determining the final price of the reference obligation for purposes of cash settlement. (ISDA 2003 Term: Valuation Method). For example, Market, Highest etc.'} valuationMethod: cdm::model::ValuationMethodEnum[0..1];
}
